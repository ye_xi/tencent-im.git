<?php


namespace Always\TencentIm\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Always\TencentIm\service\Message;

class MessageServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        !isset($pimple['message']) && $pimple['message'] = function ($pimple) {
            return new Message($pimple['config']);
        };
    }
}
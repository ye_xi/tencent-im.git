<?php


namespace Always\TencentIm\ServiceProviders;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Always\TencentIm\service\Account;

class AccountServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        !isset($pimple['account']) && $pimple['account'] = function ($pimple) {
            return new Account($pimple['config']);
        };
    }
}
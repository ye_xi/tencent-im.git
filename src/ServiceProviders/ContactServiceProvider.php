<?php


namespace Always\TencentIm\ServiceProviders;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Always\TencentIm\service\Contact;

class ContactServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        !isset($pimple['contact']) && $pimple['contact'] = function ($pimple) {
            return new Contact($pimple['config']);
        };
    }
}
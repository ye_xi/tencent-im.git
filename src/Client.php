<?php


namespace Always\TencentIm;

class Client
{
    private $baseUrl = "https://console.tim.qq.com/v4/";
    private $appid;
    private $key;
    private $identifier;
    private $contenttype = 'json';

    public function __construct($config)
    {
        if (!isset($config['appid']) || !isset($config['key'])) {
            throw new \InvalidArgumentException('appid or key incorrect configuration');
        }
        $this->appid = $config['appid'];
        $this->key = $config['key'];
        $this->identifier = $config['identifier'] ?? 'administrator';
    }

    /**
     * @param $url
     * @param $param
     * @return mixed
     */
    protected function sendPost($url, $param)
    {
        $userSig = $this->getAdminSig();
        $post     = sprintf($this->baseUrl . $url . "?sdkappid=%s&identifier=%s&usersig=%s&random=%s&contenttype=%s",
            $this->appid, $this->identifier, $userSig, rand(0, 4294967295), $this->contenttype);
        $headers  = ['Content-type: application/json'];
        $options  = [
            CURLOPT_HTTPHEADER => $headers
        ];
        $response = Http::post($post, json_encode($param), $options);
        $data     = json_decode($response, true);

        if ($data['ErrorCode'] != 0) {
            throw new \Exception($data['ErrorInfo'], $data['ErrorCode']);
        }
        return $data;
    }

    public function getAdminSig()
    {
        $im            = new TLSSigAPIv2($this->appid, $this->key);
        return $im->genUserSig($this->identifier);
    }
}
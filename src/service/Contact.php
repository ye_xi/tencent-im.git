<?php


namespace Always\TencentIm\service;


use Always\TencentIm\Client;

class Contact extends Client
{
    /**
     * 拉取会话列表
     * @param string $userId UserID，请求拉取该用户的会话列表
     * @param int $timeStamp 普通会话的起始时间，第一页填 0
     * @param int $startIndex 普通会话的起始位置，第一页填 0
     * @param int $topTimeStamp 置顶会话的起始时间，第一页填 0
     * @param int $topStartIndex 置顶会话的起始位置，第一页填 0
     * @param int $assistFlags 会话辅助标志位: bit 0 - 是否支持置顶会话 bit 1 - 是否返回空会话 bit 2 - 是否支持置顶会话分页
     * @return mixed
     * @throws \Exception
     */
    public function getList($userId, $timeStamp = 0, $startIndex = 0, $topTimeStamp = 0, $topStartIndex = 0, $assistFlags = 0)
    {
        $param = [
            'From_Account' => (string)$userId,
            'TimeStamp' => (int)$timeStamp,
            'StartIndex' => (int)$startIndex,
            'TopTimeStamp' => (int)$topTimeStamp,
            'TopStartIndex' => (int)$topStartIndex,
            'AssistFlags' => (int)$assistFlags,
        ];
        $url   = "recentcontact/get_list";
        return $this->sendPost($url, $param);
    }

    /**
     * 删除单个会话
     * @param string $userId 请求删除该 UserID 的会话
     * @param int $type 会话类型：1 表示 C2C 会话；2 表示 G2C 会话
     * @param array $extends
     * @return mixed
     * @throws \Exception
     */
    public function delete($userId, $type = 1, $extends = [])
    {
        $param = [
            'From_Account' => (string)$userId,
            'Type' => (int)$type,
        ];
        if (!empty($extends)) {
            $param = array_merge($extends, $param);
        }
        $url   = "recentcontact/delete";
        return $this->sendPost($url, $param);
    }
}
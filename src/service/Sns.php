<?php


namespace Always\TencentIm\service;


use Always\TencentIm\Client;

class Sns extends Client
{
    /**
     * 添加单个好友
     * @param string $userId 用户id
     * @param string $addId 好友的 UserID
     * @param string $remark 好友备注
     * @param string $groupName 好友分组
     * @param string $addType
     * @param int $forceAddFlags
     * @return mixed
     */
    public function friendAdd($userId, $addId, $remark = '', $groupName = '', $addType = 'Add_Type_Single', $forceAddFlags = 1)
    {
        $param = [
            'From_Account'  => (string)$userId,
            'AddFriendItem' => [
                [
                    'To_Account' => (string)$addId,
                    'Remark' => $remark,
                    'GroupName'  => $groupName,
                    'AddSource'  => "AddSource_Type_Follow",
                    'AddWording' => '互相关注成为新朋友,快发起语音聊聊吧'
                ]
            ],
            'AddType' => $addType,
            "ForceAddFlags" => $forceAddFlags
        ];
        $url   = "sns/friend_add";
        return $this->sendPost($url, $param);
    }

    /**
     * 拉取好友
     * @param string $userId 用户id
     * @return mixed
     */
    public function friendGet($userId)
    {
        $param = [
            'From_Account' => (string)$userId,
            'StartIndex'   => 0
        ];
        $url   = "sns/friend_get";
        return $this->sendPost($url, $param);
    }

    /**
     * 拉取指定好友
     * @param string $userId 用户id
     * @param array $friendId 好友的 UserID 列表 ["UserID_2"]
     * @return mixed
     */
    public function friendGetList($userId, array $friendId)
    {
        $param = [
            'From_Account' => (string)$userId,
            'To_Account'   => $friendId,
            'TagList' => [
                'Tag_Profile_Custom_Test',
                'Tag_Profile_IM_Image',
                'Tag_Profile_IM_Nick',
                'Tag_SNS_Custom_Test',
                'Tag_SNS_IM_Remark',
                'Tag_SNS_IM_Group'
            ]
        ];
        $url   = "sns/friend_get_list";
        return $this->sendPost($url, $param);
    }

    /**
     * 校验好友关系
     * @param string $userId 用户id
     * @param array $friendId 好友的 UserID 列表 ["id1","id2","id3","id4","id5"]
     * @param string $checkType 校验模式
     * @return mixed
     */
    public function friendCheck($userId, array $friendId, $checkType = 'CheckResult_Type_Both')
    {
        $param = [
            'From_Account' => (string)$userId,
            'To_Account' => $friendId,
            'CheckType' => $checkType
        ];
        $url   = "sns/friend_check";
        return $this->sendPost($url, $param);
    }

    /**
     * 更新单个好友
     * @param string $userId
     * @param string $friendId
     * @param array $items
     * @return mixed
     */
    public function friendUpdate($userId, $friendId, array $items)
    {
        $param = [
            'From_Account' => (string)$userId,
            'UpdateItem'   => [
                'To_Account' => (string)$friendId,
                'SnsItem' => $items
            ]
        ];
        $url   = "sns/friend_update";
        return $this->sendPost($url, $param);
    }

    /**
     * 删除好友
     * @param string $userId
     * @param array $friendId ["id1","id2","id3"]
     * @param string $deleteType Delete_Type_Single 单项删除好友，Delete_Type_Both 双向删除好友
     * @return mixed
     */
    public function friendDelete($userId, array $friendId, $deleteType = 'Delete_Type_Single')
    {
        $param = [
            'From_Account' => (string)$userId,
            'To_Account' => $friendId,
            'DeleteType' => $deleteType,
        ];
        $url   = "sns/friend_delete";
        return $this->sendPost($url, $param);
    }
}
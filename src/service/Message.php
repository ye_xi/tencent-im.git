<?php


namespace Always\TencentIm\service;


use Always\TencentIm\Client;

class Message extends Client
{
    /**
     * @param string $sync 是否同步消息到发送方, 1：把消息同步 2：消息不同步
     * @param string $toAccount 接收方
     * @param array $msgBody 消息体
     * @param string $fromAccount 指定发送方
     * @param array $offline 离线推送信息
     * @param array $extends
     * @return mixed
     */
    public function sendMsg($sync, $toAccount, array $msgBody, $fromAccount = '', $offline = [], $extends = [])
    {
        $param = [
            'SyncOtherMachine' => $sync,
            'To_Account' => $toAccount,
            'MsgRandom' => rand(0, 10000000),
            'MsgTimeStamp' => time(),
            'MsgBody' => $msgBody,
        ];
        if (!empty($fromAccount)) {
            $param = array_merge($param, ['From_Account' => (string)$fromAccount]);
        }
        if (!empty($offline)) {
            $param = array_merge($param, ['OfflinePushInfo' => $offline]);
        }
        if (!empty($extends)) {
            $param = array_merge($extends, $param);
        }
        $url   = "openim/sendmsg";
        return $this->sendPost($url, $param);
    }

    /**
     * 批量发单聊消息
     * @param string $sync 是否同步消息到发送方, 1：把消息同步 2：消息不同步
     * @param array $toAccount 接收方
     * @param array $msgBody 消息体
     * @return mixed
     */
    public function batchSendMsg($sync, array $toAccount, array $msgBody)
    {
        $param = [
            'SyncOtherMachine' => $sync,
            'To_Account'       => $toAccount,
            'MsgRandom'        => rand(0, 10000000),
            'MsgTimeStamp'     => time(),
            'MsgBody'          => $msgBody
        ];
        $url   = "openim/batchsendmsg";
        return $this->sendPost($url, $param);
    }

    /**
     * 查询单聊消息
     * @param string $fromAccount 发送方
     * @param string $toAccount 接收方
     * @param int $count 条数
     * @param int $minTime 查询开始时间
     * @param int $maxTime 查询结束时间
     * @return mixed
     */
    public function adminGetroammsg($fromAccount, $toAccount, $count, $minTime, $maxTime)
    {
        $param = [
            'From_Account' => (string)$fromAccount,
            'To_Account' => (string)$toAccount,
            'MaxCnt' => $count,
            'MinTime' => $minTime,
            'MaxTime' => $maxTime,
        ];
        $url   = "openim/admin_getroammsg";
        return $this->sendPost($url, $param);
    }

    /**
     * 设置单聊消息已读
     * @param string $reportAccount 进行消息已读的用户 UserId
     * @param string $peerAccount 进行消息已读的单聊会话的另一方用户 UserId
     * @param string $MsgReadTime 时间戳（秒），该时间戳之前的消息全部已读。若不填，则取当前时间戳
     * @return mixed
     */
    public function adminSetMsgRead($reportAccount, $peerAccount, $MsgReadTime = '')
    {
        if (!$MsgReadTime) {
            $MsgReadTime = time();
        }
        $param = [
            'Report_Account' => (string)$reportAccount,
            'Peer_Account' => (string)$peerAccount,
            'MsgReadTime' => (string)$MsgReadTime,
        ];
        $url   = "openim/admin_set_msg_read";
        return $this->sendPost($url, $param);
    }

    /**
     * @param string $type 消息类型
     * @param array $message 消息包体
     * @param string $desc 简介
     * @return array|array[]
     */
    public function formatMessage($type, $message, $desc)
    {
        switch ($type) {
            case 'text':
                $type        = "TIMTextElem";
                $messageBody = [
                    'Text' => $message
                ];
                break;
            case 'custom':
                $type        = "TIMCustomElem";
                $messageBody = [
                    'Data' => $message,
                    'Desc' => $desc
                ];
                break;
            default:
                return [];
        }
        return [
            [
                "MsgType"    => $type,
                "MsgContent" => $messageBody
            ]
        ];
    }

}
## Requirement

1. PHP >= 7.0

```shell
$ composer require "always/tencent-im:^1.0"
```

## Usage

```php
<?php

use Always\TencentIm\TencentIm;

$config = ['appid' => 'AppId', 'key' => 'Key'];
$im = new TencentIm($config);
$res = $im->account->accountCheck([['UserID' => '1']]);
var_dump($res);


$account = $im->account; // 账号管理API
$message = $im->message; // 消息API
$sns = $im->sns; // 关系链管理API

```